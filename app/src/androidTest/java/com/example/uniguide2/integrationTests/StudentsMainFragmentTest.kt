package com.example.uniguide2.integrationTests

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.example.uniguide2.MainActivity
import com.example.uniguide2.R
import com.example.uniguide2.StudentsMainFragment
import com.example.uniguide2.data.Post
import com.example.uniguide2.data.UniguideDatabase
import com.example.uniguide2.network.PostApiService
import com.example.uniguide2.repository.PostRepository
import com.example.uniguide2.viewmodel.PostViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.robolectric.annotation.LooperMode
import org.robolectric.annotation.TextLayoutMode

@RunWith(AndroidJUnit4::class)
@MediumTest
@LooperMode(LooperMode.Mode.PAUSED)
@TextLayoutMode(TextLayoutMode.Mode.REALISTIC)
@ExperimentalCoroutinesApi
class StudentsMainFragmentTest {

    private lateinit var repo: PostRepository
    private lateinit var database: UniguideDatabase
    private lateinit var viewModel: PostViewModel

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    @Rule
    @JvmField
    val activityRule = object : ActivityTestRule<MainActivity>(MainActivity::class.java) {
        override fun getActivityIntent(): Intent {
            return Intent(InstrumentationRegistry.getInstrumentation().context, MainActivity::class.java)

        }
    }

    @Rule
    fun rule() = activityRule

    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            UniguideDatabase::class.java,
            "uniguide_db").allowMainThreadQueries().build()

        repo = PostRepository(database.postDao(), PostApiService.getInstance())
        viewModel = PostViewModel(ApplicationProvider.getApplicationContext())
        rule().activity.supportFragmentManager.beginTransaction()
    }

    @After
    fun cleanUp() {
        database.close()
    }

    @Test
    fun displayPost_whenRepositoryHasData() = runBlockingTest{
        // GIVEN - One group already in the repository
        repo.savePostRoom(
            Post(1, "New Post","universities","false", 0, "user")
        )

        // WHEN - On startup
        val scenario = launchFragmentInContainer<StudentsMainFragment>(Bundle(),
            R.style.AppTheme
        )
        val navController = mock(NavController::class.java)
        scenario.onFragment {
            Navigation.setViewNavController(it.view!!, navController)
        }

        //scenario.moveToState(Lifecycle.State.CREATED)


        // THEN - Verify group is displayed on screen
        Espresso.onView(ViewMatchers.withText("New Post")).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun clickAddTaskButton_navigateToAddEditFragment() {
        // Given a user in the home screen
        val scenario = launchFragmentInContainer<StudentsMainFragment>(Bundle(),
            R.style.AppTheme
        )
        val navController = mock(NavController::class.java)
        scenario.onFragment {
            Navigation.setViewNavController(it.view!!, navController)
        }

        // When the FAB is clicked
        onView(withId(R.id.new_post_button)).perform(click())

        // Then verify that we navigate to the add screen
        verify(navController).navigate(R.id.postFragment)
    }
}
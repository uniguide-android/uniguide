package com.example.uniguide2.integrationTests

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.example.uniguide2.MainActivity
import com.example.uniguide2.NewsFragment
import com.example.uniguide2.R
import com.example.uniguide2.data.News
import com.example.uniguide2.data.UniguideDatabase
import com.example.uniguide2.network.NewsApiService
import com.example.uniguide2.repository.NewsRepository
import com.example.uniguide2.viewmodel.NewsViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.annotation.LooperMode
import org.robolectric.annotation.TextLayoutMode

@RunWith(AndroidJUnit4::class)
@MediumTest
@LooperMode(LooperMode.Mode.PAUSED)
@TextLayoutMode(TextLayoutMode.Mode.REALISTIC)
@ExperimentalCoroutinesApi
class NewsFragmentTest {

    private lateinit var repo: NewsRepository
    private lateinit var database: UniguideDatabase
    private lateinit var viewModel: NewsViewModel

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    @Rule
    @JvmField
    val activityRule = object : ActivityTestRule<MainActivity>(MainActivity::class.java) {
        override fun getActivityIntent(): Intent {
            return Intent(InstrumentationRegistry.getInstrumentation().context, MainActivity::class.java)

        }
    }

    @Rule
    fun rule() = activityRule

    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            UniguideDatabase::class.java,
            "uniguide_db").allowMainThreadQueries().build()

        repo = NewsRepository(database.newsDao(), NewsApiService.getInstance())
        viewModel = NewsViewModel(ApplicationProvider.getApplicationContext())
        rule().activity.supportFragmentManager.beginTransaction()
    }

    @After
    fun cleanUp() {
        database.close()
    }

    @Test
    fun displayPost_whenRepositoryHasData() = runBlockingTest{
        // GIVEN - One group already in the repository
        repo.saveNewsRoom(
            News(0, "News 1","News Content","user")
        )

        // WHEN - On startup
        val scenario = launchFragmentInContainer<NewsFragment>(Bundle(),
            R.style.AppTheme
        )
        val navController = Mockito.mock(NavController::class.java)
        scenario.onFragment {
            Navigation.setViewNavController(it.view!!, navController)
        }

        //scenario.moveToState(Lifecycle.State.CREATED)


        // THEN - Verify group is displayed on screen
        Espresso.onView(ViewMatchers.withText("News 1")).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

}
package com.example.uniguide2

import android.content.res.Resources
import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.example.uniguide2.data.Career
import com.example.uniguide2.eventHandler.ViewDetailHandler
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.navOptions
import androidx.navigation.ui.*
import com.example.uniguide2.SignUpAsFragments.*

const val ID_KEY = "idKey"
const val SHARED_PREFERENCE_ID = "current_user_preference_file_id"

class MainActivity : AppCompatActivity(), LoginFragment.OnFragmentInteractionListener, StudentsMainFragment.OnFragmentInteractionListener,
    PostFragment.OnFragmentInteractionListener, SignupFragment.OnFragmentInteractionListener, AgentSignUpfragment.OnFragmentInteractionListener,
    MainAgentFragment.OnFragmentInteractionListener, AgentPostFragment.OnFragmentInteractionListener, UniPostFragment.OnFragmentInteractionListener,
    MainUniversityFragment.OnFragmentInteractionListener, UniversitySignUpfragment.OnFragmentInteractionListener, NewsFragment.OnFragmentInteractionListener,
    ViewDetailHandler {

    override fun onAgentPostButtonClicked() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onUFabButtonClicked() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onAgentSignUpButtonClicked() {

    }

    private lateinit var appBarConfiguration : AppBarConfiguration
    //ViewDetailHandler Implementation
    override fun viewCareerDetails(group: Career) {
        val options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }


        }
        var args = Bundle()
        args.putSerializable("careerGroup", group)
        findNavController(R.id.my_nav_host_fragment).navigate(R.id.careerDetailFragment, args, options)
    }

    override fun onAgentLoginButtonClicked() {


    }


    override fun onFabButtonClicked() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onUniversitySignUpButtonClicked() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLoginButtonClicked() {

        /*val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        val mainFragment = StudentsMainFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.frame, mainFragment)
            .addToBackStack(null)
            .commit()*/

    }

    override fun onSignupButtonClicked() {
        /*val signupFragment = SignupFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.frame, signupFragment)
            .addToBackStack(null)
            .commit()*/

    }

    override fun onSubmitButtonClicked() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.my_nav_host_fragment) as NavHostFragment? ?: return

        // Set up Action Bar
        val navController = host.navController

        appBarConfiguration = AppBarConfiguration(navController.graph)

        val drawerLayout : DrawerLayout? = findViewById(R.id.drawer_layout)
        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.universitiesMainFragment),
            drawerLayout)

        setupActionBar(navController, appBarConfiguration)

        setupNavigationMenu(navController)

        setupBottomNavMenu(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            val dest: String = try {
                resources.getResourceName(destination.id)
            } catch (e: Resources.NotFoundException) {
                Integer.toString(destination.id)
            }

            Log.d("NavigationActivity", "Navigated to $dest")

            /* val loginFragment = LoginFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.frame, loginFragment)
            .addToBackStack(null)
            .commit()*/

        }
    }

    private fun setupBottomNavMenu(navController: NavController) {
        // TODO STEP 9.3 - Use NavigationUI to set up Bottom Nav
//        val bottomNav = findViewById<BottomNavigationView>(R.id.bottom_nav_view)
//        bottomNav?.setupWithNavController(navController)
        // TODO END STEP 9.3
    }

    private fun setupNavigationMenu(navController: NavController) {
        // TODO STEP 9.4 - Use NavigationUI to set up a Navigation View
//        // In split screen mode, you can drag this view out from the left
//        // This does NOT modify the actionbar
        val sideNavView = findViewById<NavigationView>(R.id.nav_view)
        sideNavView?.setupWithNavController(navController)
        // TODO END STEP 9.4
    }

    private fun setupActionBar(
        navController: NavController,
        appBarConfig: AppBarConfiguration
    ) {
        setupActionBarWithNavController(navController, appBarConfig)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val retValue = super.onCreateOptionsMenu(menu)
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        // The NavigationView already has these same navigation items, so we only add
        // navigation items to the menu here if there isn't a NavigationView
        if (navigationView == null) {
            menuInflater.inflate(R.menu.overflow_menu, menu)
            return true
        }
        return retValue
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(findNavController(R.id.my_nav_host_fragment))
                || super.onOptionsItemSelected(item)
    }

    // TODO STEP 9.7 - Have NavigationUI handle up behavior in the ActionBar
    // TODO STEP 9.7 - Have NavigationUI handle up behavior in the ActionBar
    override fun onSupportNavigateUp(): Boolean {

        return findNavController(R.id.my_nav_host_fragment).navigateUp(appBarConfiguration)
    }
    // TODO END STEP 9.7
}

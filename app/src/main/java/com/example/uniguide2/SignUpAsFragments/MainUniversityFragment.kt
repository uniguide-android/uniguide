package com.example.uniguide2.SignUpAsFragments


import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.ID_KEY
import com.example.uniguide2.R
import com.example.uniguide2.SHARED_PREFERENCE_ID
import com.example.uniguide2.data.University
import com.example.uniguide2.databinding.FragmentLoginBinding
import com.example.uniguide2.databinding.FragmentMainUniversityBinding
import com.example.uniguide2.viewmodel.UniversityViewModel
import com.example.uniguide2.viewmodel.UserViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_main_agent.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class MainUniversityFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private lateinit var uniBinding: FragmentMainUniversityBinding
    private lateinit var uniViewModel: UniversityViewModel
    private lateinit var options : NavOptions



    private lateinit var name: TextView
    private lateinit var website: TextView
    private lateinit var phone: TextView
    private lateinit var description: TextView
    private lateinit var Ufab: FloatingActionButton

    lateinit var sharedPref: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        /*val view= inflater.inflate(R.layout.fragment_main_university, container, false)
        name=view.username_textview
        website=view.content_textview

        phone=view.uni_news_title
        description=view.description_tv*/

        uniBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_main_university,container , false)
        val myView : View = uniBinding.root
        uniBinding.callback = this

        options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }
       /* Ufab=view.fab
        Ufab.setOnClickListener{
            findNavController().navigate(R.id.uniPostFragment, null, options)
        }*/

        uniViewModel = ViewModelProviders.of(this).get(UniversityViewModel::class.java)

        sharedPref=(listener as Activity).getSharedPreferences(SHARED_PREFERENCE_ID,Context.MODE_PRIVATE)
        val userId=sharedPref.getLong(ID_KEY,0)
        if(connected()){
            uniViewModel.findByUserId(userId)
            uniViewModel.getByUserResponse.observe(this, Observer { response ->
                response.body()?.run {
                    updateFields(this)
                }
            })
        }
        return myView
    }


    private fun updateFields(uni: University) {
        uniBinding.usernameTextview.setText(uni.name)
        uniBinding.contentTextview.setText(uni.website)
        uniBinding.uniNewsTitle.setText(uni.phoneNo)
        uniBinding.descriptionTv.setText(uni.description)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name

        fun onUFabButtonClicked()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SignupFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MainUniversityFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun connected():Boolean {

        val connectivityManager = (listener as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }
    fun uniPost(){
        findNavController().navigate(R.id.uniPostFragment, null, options)
    }
}

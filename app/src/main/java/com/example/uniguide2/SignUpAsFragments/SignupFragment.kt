package com.example.uniguide2.SignUpAsFragments

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.R
import com.example.uniguide2.data.Student
import com.example.uniguide2.data.User
import com.example.uniguide2.databinding.FragmentLoginBinding
import com.example.uniguide2.databinding.FragmentSignupBinding
import com.example.uniguide2.network.StudentApiService
import com.example.uniguide2.viewmodel.StudentViewModel
import com.example.uniguide2.viewmodel.UserViewModel
import com.google.android.material.textfield.TextInputEditText
//import kotlinx.android.synthetic.main.fragment_login.view.signup_button
import kotlinx.android.synthetic.main.fragment_signup.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SignupFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SignupFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SignupFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private lateinit var signupBinding: FragmentSignupBinding
    private lateinit var studentViewModel: StudentViewModel
    private lateinit var options : NavOptions



    private lateinit var nameEditText: TextInputEditText
    private lateinit var userNameEditText: TextInputEditText
    private lateinit var passwordEditText: TextInputEditText
    private lateinit var emailEditText: TextInputEditText
    private lateinit var schoolEditText: EditText
    private lateinit var gradeEditText: EditText
    private lateinit var aboutEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        /*val view = inflater.inflate(R.layout.fragment_signup, container, false)

        val signupButton = view.signup_button

        val userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        nameEditText = view.name_edit_text
        userNameEditText = view.signup_username_edit_text
        passwordEditText = view.signup_password_edit_text
        emailEditText = view.email_edit_text
        schoolEditText = view.school_edit_text
        gradeEditText = view.grade_edit_text
        aboutEditText = view.about_edit_text*/

        signupBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_signup,container , false)
        val myView : View = signupBinding.root
        signupBinding.callback = this

        studentViewModel = ViewModelProviders.of(this).get(StudentViewModel::class.java)


        options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }

       /* signupButton.setOnClickListener{
            val student = readFields()
            if(connected()){
                studentViewModel.registerStudent(student)
                studentViewModel.getResponse.observe(this, Observer { response ->
                    response.body()?.run {
                        findNavController().navigate(R.id.loginFragment, null, options)
                    }
                })

            }


        }*/

        return myView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onSubmitButtonClicked()
    }


    private fun readFields(): Student{
        var user = User(0, signupBinding.signupUsernameEditText.text.toString(), signupBinding.signupPasswordEditText.text.toString(), emptySet())
        return Student(0, signupBinding.nameEditText.text.toString(), signupBinding.emailEditText.text.toString(), signupBinding.schoolEditText.text.toString(),
            signupBinding.gradeEditText.text.toString(), signupBinding.aboutEditText.text.toString(), user)
    }

    private fun connected():Boolean {

        val connectivityManager = (listener as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    fun signStudent() {
        val student = readFields()
        if (connected()) {
            studentViewModel.registerStudent(student)
            studentViewModel.getResponse.observe(this, Observer { response ->
                response.body()?.run {
                    findNavController().navigate(R.id.loginFragment, null, options)
                }
            })

        }
        else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }
    }


}

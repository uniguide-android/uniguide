package com.example.uniguide2.SignUpAsFragments

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions

import com.example.uniguide2.R
import com.example.uniguide2.data.University
import com.example.uniguide2.data.User
import com.example.uniguide2.viewmodel.UniversityViewModel
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.fragment_agent_sign_up.view.*
import kotlinx.android.synthetic.main.fragment_signup.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [UniversitySignUpfragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [UniversitySignUpfragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class UniversitySignUpfragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private lateinit var uniSignBinding: FragmentUniversitySignUpBinding
    private lateinit var uniViewModel: UniversityViewModel
    private lateinit var options : NavOptions

    private lateinit var nameEditText: EditText
    private lateinit var userNameEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var confirmPasswordEditText: EditText
    private lateinit var emailEditText: EditText
    private lateinit var phoneNumberEditText: EditText
    private lateinit var addressEditText: EditText
    private lateinit var descriptionEditText: EditText
    private lateinit var uniloginButton: MaterialButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        /*val view = inflater.inflate(R.layout.fragment_university_sign_up, container, false)
        val signupButton = view.signup_button
        nameEditText = view.Name_et
        userNameEditText = view.userName_et
        passwordEditText = view.password_et
        confirmPasswordEditText = view.confirmPassword_et
        phoneNumberEditText = view.phoneNumber_et
        addressEditText = view.address_et
        descriptionEditText = view.description_et
        uniloginButton=view.loginButton*/

        uniSignBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_login,container , false)
        val myView : View = uniSignBinding.root
        uniSignBinding.callback = this

        uniViewModel = ViewModelProviders.of(this).get(UniversityViewModel::class.java)

        val options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }
       /* uniloginButton.setOnClickListener{
            val uni = readFields()
            if(connected()){
                uniViewModel.registerUniversity(uni)
                uniViewModel.insertResponse.observe(this, Observer { response ->
                    response.body()?.run {
                        findNavController().navigate(R.id.loginFragment, null, options)
                    }
                })

            }


        }*/

        return myView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onSubmitButtonClicked()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SignupFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SignupFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun readFields(): University {
        var user = User(0, uniSignBinding.userNameEt.text.toString(), uniSignBinding.passwordEt.text.toString(), emptySet())
        return University(0, uniSignBinding.NameEt.text.toString(),
            uniSignBinding.addressEt.text.toString(),
            uniSignBinding.phoneNumberEt.text.toString(),
            uniSignBinding.descriptionEt.text.toString(), user)
    }

    private fun connected():Boolean {

        val connectivityManager = (listener as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    fun signUniversity(){
        val uni = readFields()
        if(connected()){
            uniViewModel.registerUniversity(uni)
            uniViewModel.insertResponse.observe(this, Observer { response ->
                response.body()?.run {
                    findNavController().navigate(R.id.loginFragment, null, options)
                }
            })

        }
        else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }


    }
}
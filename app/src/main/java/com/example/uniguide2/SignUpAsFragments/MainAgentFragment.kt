package com.example.uniguide2.SignUpAsFragments


import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.ID_KEY
import com.example.uniguide2.R
import com.example.uniguide2.SHARED_PREFERENCE_ID
import com.example.uniguide2.data.Agent
import com.example.uniguide2.databinding.FragmentMainAgentBinding
import com.example.uniguide2.viewmodel.AgentViewModel
import com.example.uniguide2.viewmodel.UserViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_main_agent.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MainAgentFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MainAgentFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MainAgentFragment : Fragment() {

    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null


    private lateinit var mainBinding:FragmentMainAgentBinding
    private lateinit var agentViewModel: AgentViewModel
    private lateinit var options : NavOptions


    private lateinit var name: TextView
    private lateinit var email: TextView
    private lateinit var address: TextView
    private lateinit var phone:TextView
    private lateinit var description: TextView
    private lateinit var fab: FloatingActionButton

    lateinit var sharedPref: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        /*val view= inflater.inflate(R.layout.fragment_main_agent, container, false)
        name=view.username_textview
        email=view.content_textview
        address=view.uni_news_content
        phone=view.uni_news_title
        description=view.description_tv*/


        mainBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_main_agent,container , false)
        val myView : View = mainBinding.root
        mainBinding.callback = this

        options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }
        /*fab=view.fab
        fab.setOnClickListener{
            findNavController().navigate(R.id.agentPostFragment, null, options)
        }*/



        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel::class.java)

        sharedPref=(listener as Activity).getSharedPreferences(SHARED_PREFERENCE_ID,Context.MODE_PRIVATE)
        val userId=sharedPref.getLong(ID_KEY,0)
        if(connected()){
            agentViewModel.findByUserId(userId)
            agentViewModel.getByUserResponse.observe(this, Observer { response ->
                response.body()?.run {
                    updateFields(this)
                }
            })
        }
        else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }
        return myView
    }


    private fun updateFields(agent: Agent) {
        mainBinding.usernameTextview.setText(agent.name)
        mainBinding.contentTextview.setText(agent.email)
        mainBinding.uniNewsTitle.setText(agent.phoneNo)
        mainBinding.uniNewsContent.setText(agent.address)
        mainBinding.descriptionTv.setText(agent.description)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFabButtonClicked()
    }

    companion object {
           fun newInstance(param1: String, param2: String) =
            MainAgentFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun connected():Boolean {

        val connectivityManager = (listener as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    fun addNews(){
        findNavController().navigate(R.id.agentPostFragment, null, options)

    }

}

package com.example.uniguide2.SignUpAsFragments


import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.ID_KEY
import com.example.uniguide2.R
import com.example.uniguide2.SHARED_PREFERENCE_ID
import com.example.uniguide2.data.Agent
import com.example.uniguide2.data.News
import com.example.uniguide2.data.User
import com.example.uniguide2.databinding.FragmentAgentPostBinding
import com.example.uniguide2.viewmodel.AgentViewModel
import com.example.uniguide2.viewmodel.NewsViewModel
import com.example.uniguide2.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.fragment_agent_post.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
class AgentPostFragment : Fragment() {

    private lateinit var agentBinding: FragmentAgentPostBinding
    private lateinit var options : NavOptions
    private lateinit var userViewModel : UserViewModel
    private lateinit var newsViewModel : NewsViewModel
    private lateinit var agentViewModel: AgentViewModel
    private lateinit var user : User



    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private lateinit var usernameTextView: TextView
    private lateinit var title: TextView
    private lateinit var post: TextView
    private lateinit var postButton: Button

    lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        /*val view = inflater.inflate(R.layout.fragment_agent_post, container, false)
        title=view.uni_news_title
        post=view.uni_news_content
        postButton=view.postButton
        usernameTextView = view.username_textview*/

        agentBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_agent_post,container , false)
        val myView : View = agentBinding.root
        agentBinding.callback = this

        sharedPref = (listener as Activity).getSharedPreferences(SHARED_PREFERENCE_ID, Context.MODE_PRIVATE)

        val userId = sharedPref.getLong(ID_KEY, 0)

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        newsViewModel = ViewModelProviders.of(this).get(NewsViewModel::class.java)
        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel::class.java)

        if(connected()){
            userViewModel.findById(userId)
            userViewModel.getByIdResponse.observe(this, Observer { response ->
                response.body()?.run{
                    user = this
                    agentBinding.usernameTextview.text = this.username
                }
            })

        }else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }

        options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }
        /*postButton.setOnClickListener {

        }*/
        return myView
    }

    fun readFields(agent: Agent): News {
        val title = agentBinding.uniNewsTitle.text.toString()
        val post = agentBinding.uniNewsContent.text.toString()
        return News(0, title, post, agent.user.username)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onAgentPostButtonClicked()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SignupFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AgentPostFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun connected():Boolean {

        val connectivityManager = (listener as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    fun postNews(){
        if(connected()){
            agentViewModel.findByUserId(user.id)
            agentViewModel.getByUserResponse.observe(this, Observer{response ->
                val owner = this
                response.body()?.run{
                    var news = readFields(this)

                    newsViewModel.saveNews(news)
                    newsViewModel.insertResponse.observe(owner, Observer { response ->
                        response.body()?.run{
                            findNavController().navigate(R.id.mainAgentFragment, null, options)
                        }
                    })
                }

            })


        }else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }
    }

}

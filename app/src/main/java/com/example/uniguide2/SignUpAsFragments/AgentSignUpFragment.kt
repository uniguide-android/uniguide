package com.example.uniguide2.SignUpAsFragments


import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.R
import com.example.uniguide2.data.Agent
import com.example.uniguide2.data.User
import com.example.uniguide2.databinding.FragmentAgentSignUpBinding
import com.example.uniguide2.viewmodel.AgentViewModel
import com.example.uniguide2.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.fragment_agent_sign_up.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [AgentSignUpfragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [AgentSignUpfragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */

class AgentSignUpFragment : Fragment() {

    private lateinit var agentBinding:FragmentAgentSignUpBinding
    private lateinit var agentViewModel: AgentViewModel
    private lateinit var options : NavOptions

    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private lateinit var name: String
    private lateinit var userName: String
    private lateinit var password: String
    private lateinit var confirmPassword: String
    private lateinit var email: String
    private lateinit var phoneNumber: String
    private lateinit var address: String
    private lateinit var description: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        agentBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_agent_sign_up,container , false)
        val myView : View = agentBinding.root
        agentBinding.callback = this

        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel::class.java)

        // Inflate the layout for this fragment
        /*val view = inflater.inflate(R.layout.fragment_agent_sign_up, container, false)

        nameEditText = view.Name_et
        userNameEditText = view.userName_et
        passwordEditText = view.password_et
        emailEditText = view.email_et
        confirmPasswordEditText = view.confirmPassword_et
        phoneNumberEditText = view.phoneNumber_et
        addressEditText = view.address_et
        descriptionEditText = view.description_et
        agloginButton=view.loginButton*/

        name = agentBinding.NameEt.text.toString()
        userName = agentBinding.userNameEt.text.toString()
        password = agentBinding.passwordEt.text.toString()
        confirmPassword = agentBinding.confirmPasswordEt.text.toString()
        email = agentBinding.emailEt.text.toString()
        phoneNumber = agentBinding.phoneNumberEt.text.toString()
        address = agentBinding.addressEt.text.toString()
        description = agentBinding.descriptionEt.text.toString()



        options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }
        /*agloginButton.setOnClickListener{
            val agent = readFields()
            if(connected()){
                agentViewModel.registerAgent(agent)
                agentViewModel.insertResponse.observe(this, Observer { response ->
                    response.body()?.run {
                        findNavController().navigate(R.id.loginFragment, null, options)
                    }
                })

            }

        }*/

        return myView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onAgentLoginButtonClicked()
    }


    private fun readFields(): Agent? {
       var user = if(password == confirmPassword) User(0, userName, password, emptySet()) else null
        val agent = user?.let {
           Agent(0, name,
                email,
                phoneNumber,
                address,
                description, it)
        }
        return agent


    }

    private fun connected():Boolean {

        val connectivityManager = (listener as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    fun signAgent(){
        val agent = readFields()
        if(agent != null) {
            if (connected()) {
                agentViewModel.registerAgent(agent)
                agentViewModel.insertResponse.observe(this, Observer { response ->
                    response.body()?.run {
                        findNavController().navigate(R.id.loginFragment, null, options)
                    }
                })

            }
            else{
                Toast.makeText(activity,"Please connect to the Internet to proceed",Toast.LENGTH_SHORT).show()

            }

        }
        else{
            Toast.makeText(activity,"Trouble signing up...check input fields", Toast.LENGTH_SHORT).show()

        }
    }


}

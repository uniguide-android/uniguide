package com.example.uniguide2.repository

import androidx.lifecycle.LiveData
import com.example.uniguide2.data.CareerQuestion
import com.example.uniguide2.data.QuestionDao

class CareerQuestionRepository(val questionDao: QuestionDao) {
    fun insertQuestion(question:CareerQuestion){
       questionDao.insertQuestion(question.careerCode,question.careerQuestion)

    }
    fun deleteQuestion(question:CareerQuestion){
        questionDao.deleteQuestion(question.careerQuestion)
    }

    fun allQuestions(): LiveData<List<CareerQuestion>> {
        return questionDao.getAllQuestions()
    }

    fun updateQuestion(question:CareerQuestion){
        questionDao.updateGroup(question.careerCode,question.careerQuestion)
    }
    fun getQuestionByDescr(code:String):LiveData<CareerQuestion>{
        return questionDao.getQuestionByDesc(code)
    }
}
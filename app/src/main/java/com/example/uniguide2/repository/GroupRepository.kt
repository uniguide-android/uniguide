package com.example.uniguide2.repository

import androidx.lifecycle.LiveData
import com.example.uniguide2.data.Career

interface GroupRepository {
    fun insertGroup(group: Career)
    fun deleteGroup(group: Career)
    fun updateGroup(group: Career)
    fun getGroupByCode(code:String): LiveData<Career>
    fun allGroups(): LiveData<List<Career>>
    fun getAGroup(code:String): Career
}
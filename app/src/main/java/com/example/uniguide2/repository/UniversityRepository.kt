package com.example.uniguide2.repository

import com.example.uniguide2.data.University
import com.example.uniguide2.network.UniversityApiService
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import retrofit2.http.*

class UniversityRepository(private val universityApiService: UniversityApiService) {


    suspend fun findById(id: Long): Response<University> =
            withContext(Dispatchers.IO){
                universityApiService.findByIdAsync(id).await()
            }

    suspend fun findByUserId(id: Long): Response<University> =
        withContext(Dispatchers.IO){
            universityApiService.findByUserIdAsync(id).await()
        }

    suspend fun registerUniversity(newUniversity: University): Response<University> =
        withContext(Dispatchers.IO){
            universityApiService.registerUniversityAsync(newUniversity).await()
        }

    suspend fun updateUniversity(id: Long, newUniversity: University): Response<University> =
        withContext(Dispatchers.IO){
            universityApiService.updateUniversityAsync(id, newUniversity).await()
        }

    suspend fun deleteUniversity(id: Long): Response<Void> =
        withContext(Dispatchers.IO){
            universityApiService.deleteUniversityAsync(id).await()
        }
}
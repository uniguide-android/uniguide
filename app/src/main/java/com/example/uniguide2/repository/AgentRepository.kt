package com.example.uniguide2.repository


import com.example.uniguide2.data.Agent
import com.example.uniguide2.network.AgentApiService
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import retrofit2.http.*

class AgentRepository(private val agentApiService: AgentApiService) {

    suspend fun findById(id: Long): Response<Agent> =
            withContext(Dispatchers.IO){
                agentApiService.findByIdAsync(id).await()
            }

    suspend fun findByUserId(id: Long): Response<Agent> =
        withContext(Dispatchers.IO){
            agentApiService.findByUserIdAsync(id).await()
        }

    suspend fun registerAgent(newAgent: Agent): Response<Agent> =
        withContext(Dispatchers.IO){
            agentApiService.registerAgentAsync(newAgent).await()
        }

    suspend fun updateAgent(id: Long, newAgent: Agent): Response<Agent> =
        withContext(Dispatchers.IO){
            agentApiService.updateAgentAsync(id, newAgent).await()
        }

    suspend fun deleteAgent(id: Long): Response<Void> =
        withContext(Dispatchers.IO){
            agentApiService.deleteAgentAsync(id).await()
        }
}
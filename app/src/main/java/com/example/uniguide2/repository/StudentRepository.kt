package com.example.uniguide2.repository

import android.os.Bundle
import androidx.lifecycle.LiveData
import com.example.uniguide2.data.Student
import com.example.uniguide2.data.User
import com.example.uniguide2.network.PostApiService
import com.example.uniguide2.network.StudentApiService
import kotlinx.coroutines.*
import retrofit2.Response
import retrofit2.http.*

class StudentRepository (private val studentApiService: StudentApiService){

    suspend fun findById(id: Long): Response<Student> =
        withContext(Dispatchers.IO) {
            studentApiService.findByIdAsync(id).await()
        }

    suspend fun findByUserId(id: Long): Response<Student> =
        withContext(Dispatchers.IO) {
            studentApiService.findByUserIdAsync(id).await()
        }

    suspend fun registerStudent(newStudent: Student): Response<Student> =
        withContext(Dispatchers.IO) {
            studentApiService.registerStudentAsync(newStudent).await()
        }

    suspend fun updateStudent(id: Long, newStudent: Student): Response<Student> =
        withContext(Dispatchers.IO) {
            studentApiService.updateStudentAsync(id, newStudent).await()
        }

    suspend fun deleteStudent(id: Long): Response<Void> =
        withContext(Dispatchers.IO) {
            studentApiService.deleteStudentAsync(id).await()
        }


}
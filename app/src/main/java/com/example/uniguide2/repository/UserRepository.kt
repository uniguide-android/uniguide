package com.example.uniguide2.repository

import com.example.uniguide2.data.User
import com.example.uniguide2.network.UserApiService
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import retrofit2.http.Query

class UserRepository (private val userApiService: UserApiService){

    suspend fun login(username: String, password: String): Response<User> =
            withContext(Dispatchers.IO){
                userApiService.login(username, password).await()
            }
    suspend fun findById(id:Long): Response<User> =
        withContext(Dispatchers.IO){
            userApiService.findByIdAsync(id).await()
        }
}

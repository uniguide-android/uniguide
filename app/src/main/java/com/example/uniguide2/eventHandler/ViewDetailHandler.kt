package com.example.uniguide2.eventHandler

import com.example.uniguide2.data.Career

interface ViewDetailHandler {
    fun viewCareerDetails(group: Career)
}
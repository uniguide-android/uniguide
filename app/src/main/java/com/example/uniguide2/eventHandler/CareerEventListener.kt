package com.example.uniguide2.eventHandler

import com.example.uniguide2.data.Career

interface CareerEventListener {
    fun viewDetails(group : Career)
}
package com.example.uniguide2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.uniguide2.R
import com.example.uniguide2.data.News
import com.example.uniguide2.data.Post
import com.example.uniguide2.databinding.LayoutCareerItemBinding
import com.example.uniguide2.databinding.RecyclerViewNewsItemBinding
import com.example.uniguide2.eventHandler.ViewDetailHandler
import kotlinx.android.synthetic.main.recycler_view_item.view.*
import kotlinx.android.synthetic.main.recycler_view_news_item.view.*

class NewsAdapter(val context: Context): RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {
    val inflater = LayoutInflater.from(context)
    private var newsList: List<News> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {

        val bindingUtil: RecyclerViewNewsItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context) ,R.layout.recycler_view_news_item, parent, false)
        return NewsViewHolder(bindingUtil)
    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val news = newsList[position]
        holder.binding.news = news
    }

    internal fun setNews(news: List<News>){
        this.newsList = news
        notifyDataSetChanged()
    }

    inner class NewsViewHolder(binding: RecyclerViewNewsItemBinding):RecyclerView.ViewHolder(binding.root){
        val binding = binding
    }

}
package com.example.uniguide2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.uniguide2.R
import com.example.uniguide2.data.Career
import com.example.uniguide2.databinding.LayoutCareerItemBinding
import com.example.uniguide2.eventHandler.CareerEventListener
import com.example.uniguide2.eventHandler.ViewDetailHandler

private lateinit var handler : ViewDetailHandler
class CareerNameAdapter(context: Context, data:List<Career>) : RecyclerView.Adapter<CareerNameAdapter.CareerGroupHolder>(),
    CareerEventListener {

    private val context = context
    private var careerList: List<Career> = data

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CareerGroupHolder {
        val bindingUtil: LayoutCareerItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context) ,R.layout.layout_career_item, parent, false)
        bindingUtil.itemClickListener=this

        if(context is ViewDetailHandler){
            handler = context
        }

        return CareerGroupHolder(bindingUtil)

    }

    override fun getItemCount(): Int {
        return careerList.size
    }

    override fun onBindViewHolder(holder: CareerGroupHolder, position: Int) {
        val group = careerList[position]
        holder.binding.group = group
    }

    inner class CareerGroupHolder(binding: LayoutCareerItemBinding):RecyclerView.ViewHolder(binding.root){
        val binding = binding
    }

    override fun viewDetails(group: Career) {
        //Toast.makeText(context,"You clicked "+context, Toast.LENGTH_SHORT).show()
        //Log.d("hey","Clicked")

        handler.viewCareerDetails(group)

    }


}

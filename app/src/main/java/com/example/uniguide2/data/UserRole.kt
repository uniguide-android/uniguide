package com.example.uniguide2.data

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "user_role",
    foreignKeys = [
        ForeignKey(
            entity = User::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("user_id")),
        ForeignKey(
            entity = Role::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("role_id")
        )
    ])
class UserRole(@PrimaryKey val userId:Long, @PrimaryKey val roleId:Long) {
}
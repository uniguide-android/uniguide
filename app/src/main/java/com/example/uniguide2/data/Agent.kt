package com.example.uniguide2.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName="agent")
data class Agent
    (val id:Long,
     val name:String,
     val email:String,
     val phoneNo:String,
     val address:String,
     val description:String,
     val user:User):Serializable{

}
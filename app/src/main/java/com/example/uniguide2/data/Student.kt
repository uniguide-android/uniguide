package com.example.uniguide2.data

import androidx.room.Entity
import androidx.room.ForeignKey
import java.io.Serializable

data class Student(val id:Long, val name:String, val email:String, val school:String, val grade:String, val about:String, val user: User): Serializable {
}
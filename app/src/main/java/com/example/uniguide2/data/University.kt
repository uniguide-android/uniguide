package com.example.uniguide2.data

import java.io.Serializable

data class University(val id:Long, val name:String, val website:String,
                      val phoneNo:String, val description:String, val user:User):Serializable {
}
package com.example.uniguide2.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Post(@PrimaryKey val id:Long, val content:String, val type: String, val flag:String, val likes:Int, val username:String) {
}
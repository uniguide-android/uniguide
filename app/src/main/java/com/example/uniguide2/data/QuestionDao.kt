package com.example.uniguide2.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface QuestionDao {

    @Query("Insert into career_questions VALUES (:code, :question) ")
    fun insertQuestion(code: String,question: String)

    @Query("Delete from career_questions where question_description= :questionDescr")
    fun deleteQuestion(questionDescr:String)

    @Query("Select * from career_questions where question_description= :questionDescr")
    fun getQuestionByDesc(questionDescr:String): LiveData<CareerQuestion>

    @Query("Select * from career_questions")
    fun getAllQuestions(): LiveData<List<CareerQuestion>>

    @Query("Update career_questions SET career_code = :code where question_description= :questionDescr")
    fun updateGroup(code:String,questionDescr: String)

}
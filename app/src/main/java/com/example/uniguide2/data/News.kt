package com.example.uniguide2.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class News(@PrimaryKey val id:Long, val title:String, val content:String, val author:String) {
}
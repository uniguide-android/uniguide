package com.example.uniguide2.data.migrations

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.VisibleForTesting
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@VisibleForTesting
class Migration1To2() : Migration(1,2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE career_questions" +
                "('career_code' TEXT NOT NULL, 'question_description' TEXT NOT NULL, PRIMARY KEY(question_description)," +
                "FOREIGN KEY('career_code') REFERENCES career_groups('groupCode') ON DELETE CASCADE)")
    }
}
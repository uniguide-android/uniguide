package com.example.uniguide2.network

import com.example.uniguide2.data.Agent
import com.example.uniguide2.data.Student
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface AgentApiService {
    @GET("agent/{id}")
    fun findByIdAsync(@Path("id") id: Long): Deferred<Response<Agent>>
    @GET("agent/user/{id}")
    fun findByUserIdAsync(@Path("id") id: Long): Deferred<Response<Agent>>
    @POST("agent/register")
    fun registerAgentAsync(@Body newAgent: Agent): Deferred<Response<Agent>>
    @PUT("agent/{id}")
    fun updateAgentAsync(@Path("id") id: Long, @Body newAgent: Agent): Deferred<Response<Agent>>
    @DELETE("agent/{id}")
    fun deleteAgentAsync(@Path("id") id: Long): Deferred<Response<Void>>


    companion object {

        private val baseUrl = "http://192.168.137.1:8090/"

        fun getInstance(): AgentApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(AgentApiService::class.java)
        }
    }
}
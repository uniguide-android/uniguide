package com.example.uniguide2.network

import com.example.uniguide2.data.Agent
import com.example.uniguide2.data.University
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface UniversityApiService {
    @GET("university/{id}")
    fun findByIdAsync(@Path("id") id: Long): Deferred<Response<University>>
    @GET("university/user/{id}")
    fun findByUserIdAsync(@Path("id") id: Long): Deferred<Response<University>>
    @POST("university/register")
    fun registerUniversityAsync(@Body newUniversity: University): Deferred<Response<University>>
    @PUT("university/{id}")
    fun updateUniversityAsync(@Path("id") id: Long, @Body newUniversity: University): Deferred<Response<University>>
    @DELETE("university/{id}")
    fun deleteUniversityAsync(@Path("id") id: Long): Deferred<Response<Void>>


    companion object {

        private val baseUrl = "http://192.168.137.1:8090/"

        fun getInstance(): UniversityApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(UniversityApiService::class.java)
        }
    }
}
package com.example.uniguide2.network

import com.example.uniguide2.data.Student
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface StudentApiService {
    @GET("student/{id}")
    fun findByIdAsync(@Path("id") id: Long): Deferred<Response<Student>>
    @GET("student/user/{id}")
    fun findByUserIdAsync(@Path("id") id: Long): Deferred<Response<Student>>
    @POST("student/register")
    fun registerStudentAsync(@Body newStudent: Student): Deferred<Response<Student>>
    @PUT("student/{id}")
    fun updateStudentAsync(@Path("id") id: Long, @Body newStudent: Student): Deferred<Response<Student>>
    @DELETE("student/{id}")
    fun deleteStudentAsync(@Path("id") id: Long): Deferred<Response<Void>>


    companion object {

        private val baseUrl = "http://192.168.137.1:8090/"

        fun getInstance(): StudentApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(StudentApiService::class.java)
        }
    }
}
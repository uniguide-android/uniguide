package com.example.uniguide2.network

import com.example.uniguide2.data.Career
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface CareerApiService {
    @GET("career/all")
    fun findAllAsync(): Deferred<Response<List<Career>>>
    @GET("career/{id}")
    fun findByIdAsync(@Path("id") id: String): Deferred<Response<Career>>
    @POST("career/save")
    fun saveCareerGroupAsync(@Body newCareer: Career): Deferred<Response<Career>>
    @PUT("career/{id}")
    fun updateCareerGroupAsync(@Path("id") id: String, @Body newCareer: Career): Deferred<Response<Career>>
    @DELETE("career/{id}")
    fun deleteCareerGroupAsync(@Path("id") id: String): Deferred<Response<Void>>


    companion object {

        private val baseUrl = "http://192.168.137.1:8090/"

        fun getInstance(): CareerApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(CareerApiService::class.java)
        }
    }
}
package com.example.uniguide2.common

import android.graphics.Rect
import android.view.View
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView

class SpaceDecoration(space: Int) : RecyclerView.ItemDecoration(){

    val space = space
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.bottom = space
        outRect.top = space
        outRect.left = space
        outRect.right = space
    }

}
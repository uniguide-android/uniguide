package com.example.uniguide2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.uniguide2.data.Career
import com.example.uniguide2.databinding.FragmentCareerDetailBinding

private lateinit var binding : FragmentCareerDetailBinding
class CareerDetailFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val careerGroup = arguments?.getSerializable("careerGroup") as Career
        binding = DataBindingUtil.inflate(inflater ,R.layout.fragment_career_detail,container , false)
        val myView  = binding.root
        binding.group = careerGroup
        return myView
    }

}
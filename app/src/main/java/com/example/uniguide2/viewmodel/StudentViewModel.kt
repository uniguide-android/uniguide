package com.example.uniguide2.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.uniguide2.data.Student
import com.example.uniguide2.network.StudentApiService
import com.example.uniguide2.repository.StudentRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class StudentViewModel: ViewModel(){

    private val studentRepository: StudentRepository

    init{
        val studentApiService = StudentApiService.getInstance()
        studentRepository = StudentRepository(studentApiService)
    }

    private  val _getResponse = MutableLiveData<Response<Student>>()
    val getResponse: LiveData<Response<Student>>
        get() = _getResponse

    private  val _getByUserResponse = MutableLiveData<Response<Student>>()
    val getByUserResponse: LiveData<Response<Student>>
        get() = _getByUserResponse

    private val _updateResponse = MutableLiveData<Response<Student>>()
    val updateResponse: LiveData<Response<Student>>
        get() = _updateResponse

    private val _insertResponse = MutableLiveData<Response<Student>>()
    val insertResponse: LiveData<Response<Student>>
        get() = _insertResponse

    private val _deleteResponse = MutableLiveData<Response<Void>>()
    val deleteResponse: MutableLiveData<Response<Void>>
        get() = _deleteResponse

    fun findById(id: Long) = viewModelScope.launch {
        _getResponse.postValue(studentRepository.findById(id))
    }

    fun registerStudent(newStudent: Student) = viewModelScope.launch {
        _insertResponse.postValue(studentRepository.registerStudent(newStudent))
    }

    fun updateStudent(id: Long, newStudent: Student) = viewModelScope.launch {
        _updateResponse.postValue(studentRepository.updateStudent(id, newStudent))
    }

    fun findByUserId(id: Long) = viewModelScope.launch {
        _getByUserResponse.postValue(studentRepository.findByUserId(id))
    }

    fun deleteStudent(id: Long) = viewModelScope.launch {
        _deleteResponse.postValue(studentRepository.deleteStudent(id))
    }
}
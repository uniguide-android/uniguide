package com.example.uniguide2.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.uniguide2.data.Agent
import com.example.uniguide2.network.AgentApiService
import com.example.uniguide2.repository.AgentRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class AgentViewModel : ViewModel(){
    private val agentRepository: AgentRepository

    init{
        val agentApiService = AgentApiService.getInstance()
        agentRepository = AgentRepository(agentApiService)
    }

    private  val _getResponse = MutableLiveData<Response<Agent>>()
    val getResponse: LiveData<Response<Agent>>
        get() = _getResponse

    private  val _getByUserResponse = MutableLiveData<Response<Agent>>()
    val getByUserResponse: LiveData<Response<Agent>>
        get() = _getByUserResponse

    private val _updateResponse = MutableLiveData<Response<Agent>>()
    val updateResponse: LiveData<Response<Agent>>
        get() = _updateResponse

    private val _insertResponse = MutableLiveData<Response<Agent>>()
    val insertResponse: LiveData<Response<Agent>>
        get() = _insertResponse

    private val _deleteResponse = MutableLiveData<Response<Void>>()
    val deleteResponse: MutableLiveData<Response<Void>>
        get() = _deleteResponse

    fun findById(id: Long) = viewModelScope.launch {
        _getResponse.postValue(agentRepository.findById(id))
    }

    fun registerAgent(newAgent: Agent) = viewModelScope.launch {
        _insertResponse.postValue(agentRepository.registerAgent(newAgent))
    }

    fun updateAgent(id: Long, newAgent: Agent) = viewModelScope.launch {
        _updateResponse.postValue(agentRepository.updateAgent(id, newAgent))
    }

    fun findByUserId(id: Long) = viewModelScope.launch {
        _getByUserResponse.postValue(agentRepository.findByUserId(id))
    }

    fun deleteAgent(id: Long) = viewModelScope.launch {
        _deleteResponse.postValue(agentRepository.deleteAgent(id))
    }
}
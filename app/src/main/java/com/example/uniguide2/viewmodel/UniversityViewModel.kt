package com.example.uniguide2.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.uniguide2.data.University
import com.example.uniguide2.network.UniversityApiService
import com.example.uniguide2.repository.UniversityRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class UniversityViewModel : ViewModel(){

    private val universityRepository: UniversityRepository

    init{
        val universityApiService = UniversityApiService.getInstance()
        universityRepository = UniversityRepository(universityApiService)
    }

    private  val _getResponse = MutableLiveData<Response<University>>()
    val getResponse: LiveData<Response<University>>
        get() = _getResponse

    private  val _getByUserResponse = MutableLiveData<Response<University>>()
    val getByUserResponse: LiveData<Response<University>>
        get() = _getByUserResponse

    private val _updateResponse = MutableLiveData<Response<University>>()
    val updateResponse: LiveData<Response<University>>
        get() = _updateResponse

    private val _insertResponse = MutableLiveData<Response<University>>()
    val insertResponse: LiveData<Response<University>>
        get() = _insertResponse

    private val _deleteResponse = MutableLiveData<Response<Void>>()
    val deleteResponse: MutableLiveData<Response<Void>>
        get() = _deleteResponse

    fun findById(id: Long) = viewModelScope.launch {
        _getResponse.postValue(universityRepository.findById(id))
    }

    fun registerUniversity(newUniversity: University) = viewModelScope.launch {
        _insertResponse.postValue(universityRepository.registerUniversity(newUniversity))
    }

    fun updateUniversity(id: Long, newUniversity: University) = viewModelScope.launch {
        _updateResponse.postValue(universityRepository.updateUniversity(id, newUniversity))
    }

    fun findByUserId(id: Long) = viewModelScope.launch {
        _getByUserResponse.postValue(universityRepository.findByUserId(id))
    }

    fun deleteUniversity(id: Long) = viewModelScope.launch {
        _deleteResponse.postValue(universityRepository.deleteUniversity(id))
    }
}
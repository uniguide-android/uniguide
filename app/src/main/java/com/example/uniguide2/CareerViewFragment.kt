package com.example.uniguide2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.uniguide2.adapter.CareerNameAdapter
import com.example.uniguide2.common.SpaceDecoration
import com.example.uniguide2.databinding.FragmentCareerViewBinding
import com.example.uniguide2.viewmodel.CareerGroupViewModel

private lateinit var careerNameAdapter:CareerNameAdapter

class CareerViewFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val bindingUtil: FragmentCareerViewBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_career_view, container,false)
        val careerRecyclerView = bindingUtil.careerOptions


        val viewModel = ViewModelProviders.of(this).get(CareerGroupViewModel::class.java)
        viewModel.allGroups.observe(this, Observer {
            groups -> groups?.let{
                careerNameAdapter = CareerNameAdapter(this.requireContext(),groups)
                careerRecyclerView.adapter = careerNameAdapter
                careerRecyclerView.layoutManager = GridLayoutManager(activity,2)
                careerRecyclerView.addItemDecoration(SpaceDecoration(4))
                careerRecyclerView.setHasFixedSize(true)

            }

        })


        return bindingUtil.root
    }




}
package com.example.uniguide2.repository

import androidx.lifecycle.LiveData
import com.example.uniguide2.data.Career
import java.util.LinkedHashMap

class FakeCareerGroupRepository : GroupRepository{
    private lateinit var result:Career


    var groupsServiceData: LinkedHashMap<String, Career> = LinkedHashMap()

    override fun insertGroup(group: Career){
        groupsServiceData[group.groupCode] = group
    }

    override fun deleteGroup(group : Career){
        groupsServiceData.remove(group.groupCode)
    }

    override fun updateGroup(group:Career){
        groupsServiceData.remove(group.groupCode)
        groupsServiceData[group.groupCode] = group


    }

    override fun getAGroup(code:String):Career{
        var group = groupsServiceData[code]
        group?.let {
            result = it
        }
        return result


    }

    override fun getGroupByCode(code: String): LiveData<Career> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun allGroups(): LiveData<List<Career>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}
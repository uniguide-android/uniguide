package com.example.uniguide2.data

import androidx.lifecycle.LiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers.notNullValue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class CareerDaoTest {

    private lateinit var database: UniguideDatabase

    @Before
    fun initDb() {

        database=
                Room.databaseBuilder(
                ApplicationProvider.getApplicationContext(),
                UniguideDatabase::class.java,
                "uniguide_db").allowMainThreadQueries().build() }

    @After
    fun closeDb() = database.close()


    @Test
    fun insertGroupAndGetByCode() = runBlockingTest {
        // GIVEN - insert a group
        val group = Career("B", "Finance, Banking, Investments and Insurance","Accounting Clerk\n" +
                "Appraiser, Credit Analyst, Credit Checker, Economist","Interest in financial and investment planning and management, and providing banking and insurance services.")
        database.groupDao().insertGroup(group)

        // WHEN - Get the group by code from the database
        val loaded = database.groupDao().getCareerGroupByCode(group.groupCode)

        // THEN - The loaded data contains the expected values
        MatcherAssert.assertThat<Career>(loaded as Career, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(loaded.groupCode, CoreMatchers.`is`(group.groupCode))
        MatcherAssert.assertThat(loaded.groupName, CoreMatchers.`is`(group.groupName))
        MatcherAssert.assertThat(loaded.occupations, CoreMatchers.`is`(group.occupations))
        MatcherAssert.assertThat(loaded.description, CoreMatchers.`is`(group.description))
    }

//    @Test
//    fun updateGroupAndGetByCode() = runBlockingTest {
//        // When inserting a task
//        val original = Career("B", "Finance","Accounting Clerk, " +
//                "Appraiser","Interest in financial and investment planning and management, and providing banking and insurance services.")
//
//        database.groupDao().insertGroup(original)
//
//        // When the task is updated
//        val updated = Career("B", "Finance, Banking, Investments and Insurance","Accounting Clerk, " +
//                "Appraiser, Credit Analyst, Credit Checker, Economist","Interest in financial and investment planning and management, and providing banking and insurance services.")
//
//        //database.groupDao().updateGroup(updated.groupCode,updated.groupName,updated.occupations,updated.description)
//
//        // THEN - The loaded data contains the expected values
//        val loaded = database.groupDao().getCareerGroupByCode(original.groupCode)
////        MatcherAssert.assertThat(loaded?.groupCode, CoreMatchers.`is`(updated.groupCode))
////        MatcherAssert.assertThat(loaded?.groupName, CoreMatchers.`is`("Finance, Banking, Investments and Insurance"))
////        MatcherAssert.assertThat(loaded?.description, CoreMatchers.`is`("Interest in financial and investment planning and management, and providing banking and insurance services."))
////        MatcherAssert.assertThat(loaded?.occupations, CoreMatchers.`is`("Accounting Clerk, "+
////                               "Appraiser, Credit Analyst, Credit Checker, Economist"))
//    }
//
//    @Test
//    fun deleteGroupByCodeAndGet() = runBlockingTest {
//        // Given a group inserted
//        val group = Career("B", "Finance","Accounting Clerk, " +
//                "Appraiser","Interest in financial and investment planning and management, and providing banking and insurance services.")
//
//        database.groupDao().insertGroup(group)
//
//        // When deleting a task by id
//        //database.groupDao().deleteGroup(group.groupCode)
//
//        // THEN - The object returned is null
//        val getGroup = database.groupDao().getCareerGroupByCode(group.groupCode)
//        MatcherAssert.assertThat(getGroup, CoreMatchers.nullValue())
//    }
//    @Test
//    fun searchCodeAndGet()=runBlockingTest{
//        val group = Career("B", "Finance","Accounting Clerk, " +
//                "Appraiser","Interest in financial and investment planning and management, and providing banking and insurance services.")
//        database.groupDao().insertGroup(group)
//
//        //val result : LiveData<Career> = database.groupDao().getGroupByCode(group.groupCode)
//
//
//
//        //MatcherAssert.assertThat(result, notNullValue())
//
//
//    }
//    @Test
//    fun getAllGroups() = runBlockingTest {
//        val group = Career("B", "Finance","Accounting Clerk, " +
//                "Appraiser","Interest in financial and investment planning and management, and providing banking and insurance services.")
//        database.groupDao().insertGroup(group)
//
//        //val result = database.groupDao().getGroupByCode(group.groupCode)
//
//        //MatcherAssert.assertThat(result, CoreMatchers.notNullValue())
//    }





}